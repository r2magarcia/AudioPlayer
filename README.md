## Reproductor de audio retro

#### Funcionalidad

El reproductor funciona con los botones del GameBoy, los cuales cumplen las siguientes funciones:

| Boton            | Función                    |
| ---------------- | -------------------------- |
| a                | Reproducir o pausar música |
| Flecha Izquierda | Retroceder el audio 10 seg |
| Flecha Derecha   | Adelantar el audio 10 seg  |

